package com.medilabo.mspatient.usecases;

import com.medilabo.mspatient.infra.dto.CreatePatientCommand;
import com.medilabo.mspatient.infra.FakeAddressRepository;
import com.medilabo.mspatient.infra.FakePatientRepository;
import com.medilabo.mspatient.domain.Address;
import com.medilabo.mspatient.domain.AddressService;
import com.medilabo.mspatient.domain.Patient;
import com.medilabo.mspatient.infra.usecase.CreatePatientUsecase;
import com.medilabo.mspatient.infra.usecase.DeletePatientUsecase;
import com.medilabo.mspatient.infra.usecase.GetPatientsUsecase;
import com.medilabo.mspatient.infra.usecase.ModifyPatientUsecase;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class PatientFixture {

    private final FakePatientRepository patientRepository = new FakePatientRepository();
    private final FakeAddressRepository addressRepository = new FakeAddressRepository();
    private final AddressService addressService = new AddressService(addressRepository);
    private final CreatePatientUsecase createPatientUsecase = new CreatePatientUsecase(patientRepository, addressService);
    private final ModifyPatientUsecase modifyPatientUsecase = new ModifyPatientUsecase(patientRepository, addressService);
    private final DeletePatientUsecase deletePatientUsecase = new DeletePatientUsecase(patientRepository);
    private final GetPatientsUsecase getPatientsUseCase = new GetPatientsUsecase(patientRepository);

    public static UUID nextPatientUUID;
    public static UUID nextAddressUUID;

    private List<Patient> patients = new ArrayList<>();
    private Optional<Patient> optionalPatient;

    public void givenNextPatientUUIDToBe(String uuid) {
        nextPatientUUID = UUID.fromString(uuid);
        patientRepository.nextUUID = nextPatientUUID;
    }

    public void givenNextAddressUUIDToBe(String uuid) {
        nextAddressUUID = UUID.fromString(uuid);
        addressRepository.nextUUID = nextAddressUUID;
    }

    public void givenExistingAddress(Address address) {
        addressRepository.addresses.add(address);
    }

    public void givenExistingPatient(Patient patient) {
        patientRepository.patients.add(patient);
    }

    public void whenCreateNewPatient(CreatePatientCommand command) {
        createPatientUsecase.createPatientUsecase(command);
    }

    public void whenUpdatePatientInfo(Patient updatedPatientInfo) {
        modifyPatientUsecase.modifyPatientUsecase(updatedPatientInfo);
    }

    public void whenDeleteAPatientById(UUID id) {
        deletePatientUsecase.deletePatientUsecase(id);
    }

    public void whenFetchingAllPatients() {
        patients = getPatientsUseCase.getAllPatients();
    }

    public void whenFetchingPatientByItsId(UUID id) {
        optionalPatient = getPatientsUseCase.getPatientById(id);
    }

    public void thenPatientRepositoryShouldContain(Patient expectedPatient) {
        assertThat(patientRepository.getAll()).contains(expectedPatient);
    }

    public void thenAddressRepositoryShouldContain(Address expectedAddress) {
        assertThat(addressRepository.getAll()).contains(expectedAddress);
    }

    public void thenAddressRepositoryShouldHaveLengthOf(Integer length) {
        assertThat(addressRepository.addresses.size()).isEqualTo(length);
    }

    public void thenPatientRepositoryShouldHaveLengthOf(Integer length) {
        assertThat(patientRepository.patients.size()).isEqualTo(length);
    }

    public void thenPatientRepositoryShouldContainAll(List<Patient> expectedPatients) {
        assertThat(patients).isEqualTo(expectedPatients);
    }

    public void thenPatientShouldBe(Optional<Patient> patient) {
        assertThat(optionalPatient).isPresent();
        assertThat(optionalPatient.get()).isEqualTo(patient.get());
    }

    public void whenRequestForUpdateThenThrow(Patient updatedPatientInfo, NoSuchElementException exception) {
        assertThatThrownBy(() -> modifyPatientUsecase.modifyPatientUsecase(updatedPatientInfo))
                .isInstanceOf(exception.getClass())
                .hasMessageContaining(exception.getMessage());
    }

    public void whenDeleteANotKnownPatientThenThrow(UUID id, NoSuchElementException exception) {
        assertThatThrownBy(() -> deletePatientUsecase.deletePatientUsecase(id))
                .isInstanceOf(exception.getClass())
                .hasMessageContaining(exception.getMessage());
    }
}
