package com.medilabo.mspatient.usecases;

import com.medilabo.mspatient.domain.Address;
import com.medilabo.mspatient.domain.Patient;
import org.junit.jupiter.api.*;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Tag("UnitTest")
public class GetPatientsUsecaseTest {
    private PatientFixture fixture;

    @BeforeEach
    void setUp() {
        fixture = new PatientFixture();
    }

    @Nested
    @DisplayName("Feature: Get all patients")
    class GetPatients {
        @Test
        void itShouldReturnAllPatients() {
            // Given
            fixture.givenExistingPatient(new Patient(
                    UUID.fromString("11111111-1111-1111-1111-111111111111"),
                    "Minot",
                    "Victor",
                    LocalDate.of(2020, 5, 17),
                    "M",
                    new Address(UUID.fromString("22222222-2222-2222-2222-222222222222"), "10", "Route du fief"),
                    "100-222-3333"
            ));
            fixture.givenExistingPatient(new Patient(
                    UUID.fromString("33333333-3333-3333-3333-333333333333"),
                    "Minot",
                    "Léonard",
                    LocalDate.of(1991, 8, 16),
                    "M",
                    new Address(UUID.fromString("22222222-2222-2222-2222-222222222222"), "10", "Route du fief"),
                    "100-222-3333"
            ));

            // When
            fixture.whenFetchingAllPatients();

            // Then
            fixture.thenPatientRepositoryShouldContainAll(List.of(
                    new Patient(
                            UUID.fromString("11111111-1111-1111-1111-111111111111"),
                            "Minot",
                            "Victor",
                            LocalDate.of(2020, 5, 17),
                            "M",
                            new Address(UUID.fromString("22222222-2222-2222-2222-222222222222"), "10", "Route du fief"),
                            "100-222-3333"
                    ),
                    new Patient(
                            UUID.fromString("33333333-3333-3333-3333-333333333333"),
                            "Minot",
                            "Léonard",
                            LocalDate.of(1991, 8, 16),
                            "M",
                            new Address(UUID.fromString("22222222-2222-2222-2222-222222222222"), "10", "Route du fief"),
                            "100-222-3333"
                    )
            ));
        }

        @Test
        void itShouldReturnAPatientByItsId() {
            // Given
            fixture.givenExistingPatient(new Patient(
                    UUID.fromString("11111111-1111-1111-1111-111111111111"),
                    "Minot",
                    "Victor",
                    LocalDate.of(2020, 5, 17),
                    "M",
                    new Address(UUID.fromString("22222222-2222-2222-2222-222222222222"), "10", "Route du fief"),
                    "100-222-3333"
            ));
            fixture.givenExistingPatient(new Patient(
                    UUID.fromString("33333333-3333-3333-3333-333333333333"),
                    "Minot",
                    "Léonard",
                    LocalDate.of(1991, 8, 16),
                    "M",
                    new Address(UUID.fromString("22222222-2222-2222-2222-222222222222"), "10", "Route du fief"),
                    "100-222-3333"
            ));

            // When
            fixture.whenFetchingPatientByItsId(UUID.fromString("11111111-1111-1111-1111-111111111111"));

            // Then
            fixture.thenPatientShouldBe(Optional.of(new Patient(
                    UUID.fromString("11111111-1111-1111-1111-111111111111"),
                    "Minot",
                    "Victor",
                    LocalDate.of(2020, 5, 17),
                    "M",
                    new Address(UUID.fromString("22222222-2222-2222-2222-222222222222"), "10", "Route du fief"),
                    "100-222-3333"
            )));

        }
    }
}
