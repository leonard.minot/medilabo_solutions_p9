package com.medilabo.mspatient.usecases;

import com.medilabo.mspatient.domain.Address;
import com.medilabo.mspatient.domain.Patient;
import org.junit.jupiter.api.*;

import java.time.LocalDate;
import java.util.NoSuchElementException;
import java.util.UUID;

@Tag("UnitTest")
public class ModifyPatientUsecaseTest {
    private PatientFixture fixture;

    @BeforeEach
    void setUp() {
        fixture = new PatientFixture();
    }

    @Nested
    @DisplayName("Feature: can modify existing patient info")
    class ModifyPatientFeature {
        @Test
        void itShouldModifyExistingPatientInfoButAddress() {
            // Given
            fixture.givenExistingAddress(new Address(UUID.fromString("22222222-2222-2222-2222-222222222222"), "10", "Route du fief"));
            fixture.givenExistingPatient(new Patient(
                    UUID.fromString("11111111-1111-1111-1111-111111111111"),
                    "Minot",
                    "Victor",
                    LocalDate.of(2020, 5, 17),
                    "M",
                    new Address(UUID.fromString("22222222-2222-2222-2222-222222222222"), "10", "Route du fief"),
                    "100-222-3333"
            ));
            fixture.givenExistingPatient(new Patient(
                    UUID.fromString("33333333-3333-3333-3333-333333333333"),
                    "Minot",
                    "Léonard",
                    LocalDate.of(1991, 8, 16),
                    "M",
                    new Address(UUID.fromString("22222222-2222-2222-2222-222222222222"), "10", "Route du fief"),
                    "100-222-3333"
            ));

            // When
            fixture.whenUpdatePatientInfo(new Patient(
                    UUID.fromString("11111111-1111-1111-1111-111111111111"),
                    "Minot",
                    "Victoria", // Change
                    LocalDate.of(2020, 5, 18), // Change
                    "F", // Change
                    new Address(UUID.fromString("22222222-2222-2222-2222-222222222222"), "10", "Route du fief"),
                    "100-222-4444" // Change
            ));

            // Then
            fixture.thenPatientRepositoryShouldContain(new Patient(
                    UUID.fromString("11111111-1111-1111-1111-111111111111"),
                    "Minot",
                    "Victoria", // Change
                    LocalDate.of(2020, 5, 18), // Change
                    "F", // Change
                    new Address(UUID.fromString("22222222-2222-2222-2222-222222222222"), "10", "Route du fief"),
                    "100-222-4444" // Change
            ));
            fixture.thenPatientRepositoryShouldHaveLengthOf(2);
        }

        @Test
        void itShouldUpdatePatientExistingPatientAddressByCreatingANewAddress() {
            // Given
            fixture.givenExistingAddress(new Address(UUID.fromString("22222222-2222-2222-2222-222222222222"), "10", "Route du fief"));
            fixture.givenNextAddressUUIDToBe("44444444-4444-4444-4444-444444444444");

            fixture.givenExistingPatient(new Patient(
                    UUID.fromString("11111111-1111-1111-1111-111111111111"),
                    "Minot",
                    "Victor",
                    LocalDate.of(2020, 5, 17),
                    "M",
                    new Address(UUID.fromString("22222222-2222-2222-2222-222222222222"), "10", "Route du fief"),
                    "100-222-3333"
            ));

            // When
            fixture.whenUpdatePatientInfo(new Patient(
                    UUID.fromString("11111111-1111-1111-1111-111111111111"),
                    "Minot",
                    "Victor",
                    LocalDate.of(2020, 5, 17),
                    "M",
                    new Address(UUID.randomUUID(), "7", "Rue de l'aumonerie"), // Change
                    "100-222-3333"
            ));

            // Then
            fixture.thenPatientRepositoryShouldContain(new Patient(
                    UUID.fromString("11111111-1111-1111-1111-111111111111"),
                    "Minot",
                    "Victor",
                    LocalDate.of(2020, 5, 17),
                    "M",
                    new Address(UUID.randomUUID(), "7", "Rue de l'aumonerie"), // Change
                    "100-222-3333"
            ));

            fixture.thenAddressRepositoryShouldHaveLengthOf(2);

            fixture.thenAddressRepositoryShouldContain(new Address(UUID.fromString("44444444-4444-4444-4444-444444444444"), "7", "Rue de l'aumonerie"));
        }

        @Test
        void itShouldUpdateExistingPatientAddressWithAnExistingAddress() {
            // Given
            fixture.givenExistingAddress(new Address(UUID.fromString("22222222-2222-2222-2222-222222222222"), "10", "Route du fief"));
            fixture.givenExistingAddress(new Address(UUID.fromString("44444444-4444-4444-4444-444444444444"), "7", "Rue de l'aumonerie"));

            fixture.givenExistingPatient(new Patient(
                    UUID.fromString("11111111-1111-1111-1111-111111111111"),
                    "Minot",
                    "Victor",
                    LocalDate.of(2020, 5, 17),
                    "M",
                    new Address(UUID.fromString("22222222-2222-2222-2222-222222222222"), "10", "Route du fief"),
                    "100-222-3333"
            ));

            // When
            fixture.whenUpdatePatientInfo(new Patient(
                    UUID.fromString("11111111-1111-1111-1111-111111111111"),
                    "Minot",
                    "Victor",
                    LocalDate.of(2020, 5, 17),
                    "M",
                    new Address(UUID.randomUUID(), "7", "Rue de l'aumonerie"), // Change
                    "100-222-3333"
            ));

            // Then
            fixture.thenPatientRepositoryShouldContain(new Patient(
                    UUID.fromString("11111111-1111-1111-1111-111111111111"),
                    "Minot",
                    "Victor",
                    LocalDate.of(2020, 5, 17),
                    "M",
                    new Address(UUID.randomUUID(), "7", "Rue de l'aumonerie"), // Change
                    "100-222-3333"
            ));

            fixture.thenAddressRepositoryShouldHaveLengthOf(2);
        }
    }

    @Nested
    @DisplayName("Feature: throw if updated a not existing patient")
    class ThrowIfPatientDoesntExist {
        @Test
        void itShouldThrowIfPatientIdIsUnknown() {
            // Given
            fixture.givenExistingPatient(new Patient(
                    UUID.fromString("11111111-1111-1111-1111-111111111111"),
                    "Minot",
                    "Victor",
                    LocalDate.of(2020, 5, 17),
                    "M",
                    new Address(UUID.fromString("22222222-2222-2222-2222-222222222222"), "10", "Route du fief"),
                    "100-222-3333"
            ));

            // When
            // Then
            fixture.whenRequestForUpdateThenThrow(new Patient(
                    UUID.fromString("33333333-3333-3333-3333-333333333333"),
                    "Minot",
                    "Léonard",
                    LocalDate.of(1991, 8, 16),
                    "M",
                    new Address(UUID.fromString("22222222-2222-2222-2222-222222222222"), "10", "Route du fief"),
                    "100-222-3333"
            ), new NoSuchElementException("User not found - Impossible to update."));

        }
    }
}
