package com.medilabo.mspatient.domain;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PatientRepository {
    void savePatient(Patient command);
    List<Patient> getAll();
    Optional<Patient> getById(UUID id);
    void update(Patient patient);
    void delete(UUID id);
}
