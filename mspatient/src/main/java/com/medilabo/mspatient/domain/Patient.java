package com.medilabo.mspatient.domain;

import java.time.LocalDate;
import java.util.UUID;

public record Patient(
        UUID id,
        String lastName,
        String firstName,
        LocalDate dateOfBirth,
        String gender,
        Address address,
        String phoneNumber
) {
}
