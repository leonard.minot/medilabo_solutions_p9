package com.medilabo.mspatient.domain;

import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AddressService {
    private final AddressRepository addressRepository;

    public AddressService(AddressRepository addressRepository) {
        this.addressRepository = addressRepository;
    }

    public Address getOrCreateAddress(Address givenAddress) {
        Optional<Address> maybeAddress = addressRepository.getByAddress(
                givenAddress.number(),
                givenAddress.streetName()
        );

        return maybeAddress.orElseGet(() -> addressRepository.createAddress(givenAddress));
    }
}
