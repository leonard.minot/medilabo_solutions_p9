package com.medilabo.mspatient.infra.usecase;

import com.medilabo.mspatient.domain.Patient;
import com.medilabo.mspatient.domain.PatientRepository;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public class GetPatientsUsecase {
    private final PatientRepository patientRepository;

    public GetPatientsUsecase(PatientRepository patientRepository) {
        this.patientRepository = patientRepository;
    }

    public List<Patient> getAllPatients() {
        return patientRepository.getAll();
    }

    public Optional<Patient> getPatientById(UUID id) {
        return patientRepository.getById(id);
    }
}
