package com.medilabo.mspatient.infra.dto;

import com.medilabo.mspatient.domain.Address;

import java.time.LocalDate;

public record CreatePatientCommand(
        String lastName,
        String firstName,
        LocalDate dateOfBirth,
        String gender,
        Address address,
        String phoneNumber

) {
}
