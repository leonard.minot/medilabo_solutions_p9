package com.medilabo.mspatient.infra.repository;

import com.medilabo.mspatient.domain.Patient;
import com.medilabo.mspatient.domain.PatientRepository;
import com.medilabo.mspatient.infra.entity.PatientEntity;
import com.medilabo.mspatient.infra.repository.jpa.PatientJpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public class PatientsPostGresRepository implements PatientRepository {

    private final PatientJpaRepository patientJpaRepository;

    public PatientsPostGresRepository(PatientJpaRepository patientJpaRepository) {
        this.patientJpaRepository = patientJpaRepository;
    }

    @Override
    public void savePatient(Patient command) {
        this.patientJpaRepository.save(PatientEntity.from(command));
    }

    @Override
    public List<Patient> getAll() {
        return this.patientJpaRepository.findAllBy().stream().map(PatientEntity::toDomain).toList();
    }

    @Override
    public Optional<Patient> getById(UUID id) {
        return this.patientJpaRepository.findById(id).map(PatientEntity::toDomain);
    }

    @Override
    public void update(Patient patient) {
        this.patientJpaRepository.save(PatientEntity.from(patient));
    }

    @Override
    public void delete(UUID id) {
        this.patientJpaRepository.deleteById(id);
    }
}
