package com.medilabo.mspatient.infra.controller;

import com.medilabo.mspatient.domain.Patient;
import com.medilabo.mspatient.infra.dto.CreatePatientCommand;
import com.medilabo.mspatient.infra.usecase.CreatePatientUsecase;
import com.medilabo.mspatient.infra.usecase.DeletePatientUsecase;
import com.medilabo.mspatient.infra.usecase.GetPatientsUsecase;
import com.medilabo.mspatient.infra.usecase.ModifyPatientUsecase;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
public class PatientRestController {
    private final CreatePatientUsecase createPatientUsecase;
    private final ModifyPatientUsecase modifyPatientUsecase;
    private final DeletePatientUsecase deletePatientUsecase;
    private final GetPatientsUsecase getPatientsUsecase;

    public PatientRestController(CreatePatientUsecase createPatientUsecase, ModifyPatientUsecase modifyPatientUsecase, DeletePatientUsecase deletePatientUsecase, GetPatientsUsecase getPatientsUsecase) {
        this.createPatientUsecase = createPatientUsecase;
        this.modifyPatientUsecase = modifyPatientUsecase;
        this.deletePatientUsecase = deletePatientUsecase;
        this.getPatientsUsecase = getPatientsUsecase;
    }

    @PostMapping("api/v1/patient")
    public void createPatient(@RequestBody CreatePatientCommand command) {
        createPatientUsecase.createPatientUsecase(new CreatePatientCommand(
                command.lastName(),
                command.firstName(),
                command.dateOfBirth(),
                command.gender(),
                command.address(),
                command.phoneNumber()
        ));
    }

    @PutMapping("api/v1/patient")
    public void modifyPatient(@RequestBody Patient patientToUpdate) {
        modifyPatientUsecase.modifyPatientUsecase(patientToUpdate);
    }

    @DeleteMapping("api/v1/patient/{id}")
    public void deletePatient(@PathVariable String id) {
        deletePatientUsecase.deletePatientUsecase(UUID.fromString(id));
    }

    @GetMapping("api/v1/patients")
    public List<Patient> getAllPatients() {
        return getPatientsUsecase.getAllPatients();
    }

    @GetMapping("api/v1/patient/{id}")
    public Optional<Patient> getPatientById(@PathVariable String id) {
        return getPatientsUsecase.getPatientById(UUID.fromString(id));
    }


}
