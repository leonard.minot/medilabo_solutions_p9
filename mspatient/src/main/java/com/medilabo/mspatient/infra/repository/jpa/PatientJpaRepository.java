package com.medilabo.mspatient.infra.repository.jpa;

import com.medilabo.mspatient.infra.entity.PatientEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface PatientJpaRepository extends CrudRepository<PatientEntity, UUID> {
    List<PatientEntity> findAllBy();
}
