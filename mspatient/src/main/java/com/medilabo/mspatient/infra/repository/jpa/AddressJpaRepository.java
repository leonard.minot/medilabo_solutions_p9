package com.medilabo.mspatient.infra.repository.jpa;

import com.medilabo.mspatient.infra.entity.AddressEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface AddressJpaRepository extends CrudRepository<AddressEntity, UUID> {
    List<AddressEntity> findAllBy();
    Optional<AddressEntity> findByNumberEqualsAndStreetEquals(String number, String street);
}
