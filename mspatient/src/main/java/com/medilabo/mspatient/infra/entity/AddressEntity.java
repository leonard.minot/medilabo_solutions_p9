package com.medilabo.mspatient.infra.entity;

import com.medilabo.mspatient.domain.Address;
import jakarta.persistence.*;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Entity
@Table( name = "address")
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class AddressEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Column(name = "id")
    private UUID id;

    @Column(
            name = "number"
    )
    private String number;

    @Column(
            name = "street",
            nullable = false
    )
    private String street;

    public static AddressEntity from(Address address) {
        return new AddressEntity(
                address.id(),
                address.number(),
                address.streetName()
        );
    }

    public Address toDomain() {
        return new Address(
                this.id,
                this.number,
                this.street
        );
    }

    public AddressEntity(UUID id, String number, String street) {
        this.id = id;
        this.number = number;
        this.street = street;
    }
}
