package com.medilabo.mspatient.infra.usecase;

import com.medilabo.mspatient.infra.dto.CreatePatientCommand;
import com.medilabo.mspatient.domain.*;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class CreatePatientUsecase {
    private final PatientRepository patientRepository;
    private final AddressService addressService;

    public CreatePatientUsecase(PatientRepository patientRepository, AddressService addressService) {
        this.patientRepository = patientRepository;
        this.addressService = addressService;
    }

    public void createPatientUsecase(CreatePatientCommand command) {
        Address patientAddress = addressService.getOrCreateAddress(command.address());

        this.patientRepository.savePatient(new Patient(
                UUID.randomUUID(),
                command.lastName(),
                command.firstName(),
                command.dateOfBirth(),
                command.gender(),
                patientAddress,
                command.phoneNumber()
        ));
    }
}
