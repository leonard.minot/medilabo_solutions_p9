package com.medilabo.mspatient.infra.repository;

import com.medilabo.mspatient.domain.Address;
import com.medilabo.mspatient.domain.AddressRepository;
import com.medilabo.mspatient.infra.entity.AddressEntity;
import com.medilabo.mspatient.infra.repository.jpa.AddressJpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public class AddressPostGresRepository implements AddressRepository {

    private final AddressJpaRepository addressJpaRepository;

    public AddressPostGresRepository(AddressJpaRepository addressJpaRepository) {
        this.addressJpaRepository = addressJpaRepository;
    }

    @Override
    public Address createAddress(Address address) {
        AddressEntity savedAddress = this.addressJpaRepository.save(AddressEntity.from(address));
        return savedAddress.toDomain();
    }

    @Override
    public List<Address> getAll() {
       return this.addressJpaRepository.findAllBy().stream().map(AddressEntity::toDomain).toList();
    }

    @Override
    public Optional<Address> getById(UUID id) {
        return this.addressJpaRepository.findById(id).map(AddressEntity::toDomain);
    }

    @Override
    public Optional<Address> getByAddress(String number, String street) {
        return this.addressJpaRepository.findByNumberEqualsAndStreetEquals(number, street).map(AddressEntity::toDomain);
    }
}
