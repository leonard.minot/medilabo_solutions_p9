package com.medilabo.msfront;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsfrontV2Application {

    public static void main(String[] args) {
        SpringApplication.run(MsfrontV2Application.class, args);
    }

}
