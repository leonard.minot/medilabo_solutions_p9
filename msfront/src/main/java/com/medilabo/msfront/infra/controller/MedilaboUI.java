package com.medilabo.msfront.infra.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MedilaboUI {

    @GetMapping("/patients")
    public String patientList() {
        return "patient-list";
    }
}
